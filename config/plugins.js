module.exports = {
  //
  graphql: {
    enabled: true,
    config: {
      endpoint: "/graphql",
      shadowCRUD: true,
      playgroundAlways: true,
      depthLimit: 90,
      defaultLimit: 100,
      maxLimit: 200,
      amountLimit: 100,
      apolloServer: {
        tracing: false,
      },
    },
  },
};
